from functools import lru_cache
from rest.models.Config import Config

class Settings(Config):
    class Config:
        env_file = ".env"

@lru_cache()
def get_config() -> Settings:
    return Settings()
