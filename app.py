from rest import rest
from config import get_config

cfg = get_config()
app = rest.create_app(cfg)