from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from .helpers.jwt import AuthJwt
from .helpers.db import DBSession
from .routers.init import init_router

def init(app: FastAPI):
    # init DB
    db = DBSession(app.state.config)
    db.connect()
    app.state.db = db.db
    # init router
    init_router(app)

    # init jwt
    jwt = AuthJwt(app.state.config)
    app.state.jwt = jwt

def create_app(config):
    app = FastAPI()
    origins = ["*"]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.state.config = config
    init(app)

    # init_dao(app)
    # init_services(app)
    # init_views(app)

    return app