from fastapi import Request, APIRouter, Header
from typing import Optional
from jose import jwt

# from ..models import user as UserModel, Responses
from ..models.FastAPI_helper import FastAPI_M
router = APIRouter()

# Author: Chertkov. V.G
# Date create: 23.03.2022
# Description: Функция устанавливает коллекцию пользователя
@router.post("/collection/set")
async def setCollection( req: Request, Authorization: Optional[str] = Header(None)):
    # print(Authorization)
    app: FastAPI_M = req.app
    if not Authorization:
        return {"Error": "Access denied!!"}
    try:
        payload = app.state.jwt.decode_token(Authorization)
        return {"portfolio": "Ok"}

    # JWTError:
    # ExpiredSignatureError: If
    #
    # JWTClaimsError: If
    except jwt.JWTError as e:
        return {"portfolio": "Ok"}

    except jwt.ExpiredSignatureError as e:
        return {"Error": e}

    else:
        return {"portfolio": "Ok"}

    return {"portfolio": "Ok"}

    # payload = app.state.services.JWT.decode_token(Authorization)



# Author: Chertkov. V.G.
# Date create: 23.03.2022
# Description: Функция отдает коллекцию пользователя
@router.get("/collection/get")
async def setCollection( req: Request, Auhorization: Optional[str] = Header(None)):
    app: FastAPI_M = req.app

    return {"portfolio": "Ok"}
    # payload = app.state.services.JWT.decode_token(Authorization)