from fastapi import FastAPI
from .portfolio import router as portfolio_router
from .auth import router as auth_router
from .schedule import router as  schedule_router


def init_router(app: FastAPI):
    app.include_router(auth_router, tags=["Auth"], prefix="/auth")
    app.include_router(portfolio_router, tags=["API"], prefix="/api")
    app.include_router(schedule_router, tags=["API"], prefix="/api")

