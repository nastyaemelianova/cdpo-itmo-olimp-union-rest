from fastapi import Request, APIRouter, Header
from typing import Optional

from ..models import User as UserModel, Responses
from ..models.FastAPI_helper import FastAPI_M

router = APIRouter()


# Author: Chertkov. V.G.
# Date create: 18.03.2022
# Description: Функция возвращает объект пользователя по jwt зашитому в поле Authorization заголовка http запроса.
@router.get("/user")
async def getUserByJWT( req: Request, Authorization: Optional[str] = Header(None)):
    app: FastAPI_M = req.app
    # if token is valid
    try:
        payload = app.state.services.JWT.decode_token(Authorization)
        print(payload['sub'])

        # user: Responses.LoginSuccess = await app.state.services.user.login(user)
        user = await app.state.services.user.getUser(payload['sub'])

        return {"user": user}

    except Exception as e:
        return {'status': 500, 'msg': e}


@router.post("/decode_jwt", status_code=200, response_model=Responses.JWTDecode)
async def decode(jwt: str, req: Request):
    app: FastAPI_M = req.app
    return app.state.services.JWT.decode_and_validate_any(jwt)



#TODO forgot password (email) -> "Email sent" or smth. No other data:
# send email with link with uuid

#TODO forgot password callback (uuid):
# replace pwd(md5), DO NOT return tokens