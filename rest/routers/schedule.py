from fastapi import Request, APIRouter, Header
from typing import Optional
import pprint
from ..models.FastAPI_helper import FastAPI_M

router = APIRouter()


@router.get("/schedule/campus")
async def getCampus(req: Request, Authorization: Optional[str] = Header(None)):
    app: FastAPI_M = req.app

    cursor = app.state.db.get_collection('campus').find({})
    for document in await cursor.to_list(length=100):
        pprint.pprint(document)

    return {'result': 'ok'}

