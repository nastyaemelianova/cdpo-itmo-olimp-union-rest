from config import get_config
from pymongo import MongoClient
import datetime

cfg = get_config()
print(cfg.mongo_url)
client = MongoClient(cfg.mongo_url)
db = client[cfg.mg_db]
# id = ObjectId()

db.campus.remove({})

obj = {
        "_id": 1,
        "name": "Ломоносова",
        "address": "Санкт-Петербург, ул. Ломоносова 9",
        "classrooms": [{"_id": 10,  "num_class": "1020Б"},
                       {"_id": 11,  "num_class": "1121"},
                       {"_id": 12,  "num_class": "3404"}],
        "date": datetime.datetime.utcnow()
}

obj_too = {
        "_id": 2,
        "name": "Кронверский проспект дом 49",
        "address": "Санкт-Петербург, Кронверский проспект дом 49",
        "classrooms": [{"_id": 20,  "num_class": "511"},
                       {"_id": 21,  "num_class": "505"},
                       {"_id": 22,  "num_class": "515"}],
        "date": datetime.datetime.utcnow()
}

_id = db.campus.insert_one(obj).inserted_id
print(_id)
_id = db.campus.insert_one(obj_too).inserted_id
print(_id)

result = db.campus.find({})
for i in result:
    print('Result:', i)


